package factory.components;

public class Car {
    public Body body;
    public Accessory accessory;
    public Motor motor;
    private int id;

    public Car(Body _body, Accessory _accessory, Motor _motor, int _id) {
        body = _body;
        accessory = _accessory;
        motor = _motor;
        id = _id;
    }

    public int getID() {
        return id;
    }
}
