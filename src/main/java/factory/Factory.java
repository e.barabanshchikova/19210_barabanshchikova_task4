package factory;

import factory.components.Accessory;
import factory.components.Body;
import factory.components.Car;
import factory.components.Motor;
import factory.storage.Storage;
import factory.workers.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import threadpool.ThreadPool;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Factory {
    public static final TimeSettingsForWorkers timeSet = new TimeSettingsForWorkers();
    private final Storage<Motor> motorStorage;
    private final Storage<Body> bodyStorage;
    private final Storage<Accessory> accessoryStorage;
    private final Storage<Car> carStorage;
    private final ThreadPool threadPool;

    public static Logger logger = LoggerFactory.getLogger(Factory.class);

    public Factory() {
        FileInputStream fis;
        Properties property = new Properties();
        try {
            fis = new FileInputStream("src/main/resources/configure.properties");
            property.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }

        motorStorage = new Storage<Motor>(Integer.parseInt(property.get("StorageMotorSize").toString()));
        bodyStorage = new Storage<Body>(Integer.parseInt(property.get("StorageBodySize").toString()));
        accessoryStorage = new Storage<Accessory>(Integer.parseInt(property.get("StorageAccessorySize").toString()));
        carStorage = new Storage<Car>(Integer.parseInt(property.get("StorageAutoSize").toString()));

        logger.info("MotorStore size {}, BodyStore size {}, AccessoryStore size {}, CarStore size {}",
                motorStorage.getMaxSize(), bodyStorage.getMaxSize(),
                accessoryStorage.getMaxSize(), carStorage.getMaxSize());

        int countAccessorySupp = Integer.parseInt(property.get("AccessorySuppliers").toString());
        int countMotorSupp = Integer.parseInt(property.get("MotorSuppliers").toString());
        int countBodySupp = Integer.parseInt(property.get("BodySuppliers").toString());
        int countCarCreat = Integer.parseInt(property.get("Workers").toString());
        int countDealCreat = Integer.parseInt(property.get("Dealers").toString());

        ViewController.start(new View(bodyStorage, accessoryStorage, motorStorage, carStorage));

        threadPool = new ThreadPool(countAccessorySupp + countBodySupp + countCarCreat +
                countDealCreat + countMotorSupp + 1);

        for (int i = 0; i < countAccessorySupp; ++i) {
            threadPool.execute(new AccessorySupplier(accessoryStorage));
        }
        for (int i = 0; i < countBodySupp; ++i) {
            threadPool.execute(new BodySupplier(bodyStorage));
        }
        for (int i = 0; i < countMotorSupp; ++i) {
            threadPool.execute(new MotorSupplier(motorStorage));
        }
        for (int i = 0; i < countCarCreat; ++i) {
            threadPool.execute(new Worker(motorStorage, bodyStorage, accessoryStorage, carStorage));
        }
        threadPool.execute(new WorkerController(carStorage));
        for (int i = 0; i < countDealCreat; ++i) {
            threadPool.execute(new Dealer(carStorage));
        }
    }
}
