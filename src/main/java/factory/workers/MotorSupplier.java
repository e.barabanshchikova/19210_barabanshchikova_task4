package factory.workers;

import factory.Factory;
import factory.components.Motor;
import factory.storage.Storage;

import java.util.concurrent.TimeUnit;

public class MotorSupplier extends Supplier<Motor> {

    public MotorSupplier(Storage<Motor> curStorage) {
        super(curStorage);
    }

    @Override
    public Motor create() {
        try {
            TimeUnit.SECONDS.sleep(Factory.timeSet.getTimeForMotor());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new Motor((int) (Math.random() * 1000));
    }
}
