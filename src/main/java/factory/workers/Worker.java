package factory.workers;

import factory.Factory;
import factory.components.Accessory;
import factory.components.Body;
import factory.components.Car;
import factory.components.Motor;
import factory.storage.Storage;

import java.util.concurrent.TimeUnit;

public class Worker implements Runnable {
    private final Storage<Motor> motorStorage;
    private final Storage<Body> bodyStorage;
    private final Storage<Accessory> accessoryStorage;
    private final Storage<Car> carStorage;
    public final Object syncObject = WorkerController.syncObject;

    public Worker(Storage<Motor> motorStorage, Storage<Body> bodyStorage, Storage<Accessory> accessoryStorage,
                  Storage<Car> carStorage) {
        this.accessoryStorage = accessoryStorage;
        this.bodyStorage = bodyStorage;
        this.carStorage = carStorage;
        this.motorStorage = motorStorage;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (syncObject) {
                try {
                    syncObject.wait(); // ждем пока кто-нибудь не сделает запрос на получение новой детали
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }

            Body body = bodyStorage.getDetail();
            Motor motor = motorStorage.getDetail();
            Accessory accessory = accessoryStorage.getDetail();
            try {
                TimeUnit.SECONDS.sleep(Factory.timeSet.getTimeForMachine());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Car car = new Car(body, accessory, motor, (int) (Math.random() * 1000));

            Factory.logger.info("Worker " + Thread.currentThread().getId() + " собрал автомобиль: " + car.getID());

            carStorage.setDetail(car);
        }
    }
}
