package factory.workers;

public class TimeSettingsForWorkers {
    private long timeForAccessory;
    private long timeForBody;
    private long timeForMotor;
    private long timeForMachine;
    private long timeForSaleCar;

    public TimeSettingsForWorkers() {
        timeForAccessory = 2;
        timeForBody = 2;
        timeForMachine = 1;
        timeForMotor = 2;
        timeForSaleCar = 4;
    }

    public long getTimeForAccessory() {
        return timeForAccessory;
    }

    public long getTimeForMotor() {
        return timeForMotor;
    }

    public long getTimeForBody() {
        return timeForBody;
    }

    public long getTimeForMachine() {
        return timeForMachine;
    }

    public long getTimeForSaleCar() { return timeForSaleCar; }

    public void setTimeForMachine(long timeForMachine) {
        this.timeForMachine = timeForMachine;
    }

    public void setTimeForAccessory(long timeForAccessory) {
        this.timeForAccessory = timeForAccessory;
    }

    public void setTimeForBody(long timeForBody) {
        this.timeForBody = timeForBody;
    }

    public void setTimeForMotor(long timeForMotor) {
        this.timeForMotor = timeForMotor;
    }

    public void setTimeForSaleCar(long timeForSaleCar) { this.timeForSaleCar = timeForSaleCar; }
}
