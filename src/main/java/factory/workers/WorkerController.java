package factory.workers;


import factory.Factory;
import factory.components.Car;
import factory.storage.Storage;

public class WorkerController implements Runnable {
    public static final Object dealerSyncObj = new Object();
    private final Storage<Car> carStorage;
    public static final Object syncObject = new Object();

    public WorkerController(Storage<Car> carStorage) {
        this.carStorage = carStorage;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (dealerSyncObj) {
                // если же запросов больше нету, то засыпаем
                while (carStorage.getSize() >= carStorage.getMaxSize() / 2) {
                    try {
                        Factory.logger.info("WorkerControl {} уснул", Thread.currentThread().getId());
                        dealerSyncObj.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (carStorage.getSize() < carStorage.getMaxSize() / 2) {
                    synchronized (syncObject) {
                        syncObject.notify();
                    }
                }
            }
        }
    }
}
