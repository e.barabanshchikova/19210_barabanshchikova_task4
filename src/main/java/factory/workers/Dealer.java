package factory.workers;

import factory.Factory;
import factory.components.Car;
import factory.storage.Storage;

import java.util.concurrent.TimeUnit;

public class Dealer implements Runnable {
    private final Storage<Car> carStorage;
    private final Object syncObj = WorkerController.dealerSyncObj;

    public Dealer(Storage<Car> carStorage) {
        this.carStorage = carStorage;
    }

    @Override
    public void run() {
        while (true) { // будем раз в несколько секунд добавлять запросы, пока это будет возможно
            try {
                TimeUnit.SECONDS.sleep(Factory.timeSet.getTimeForSaleCar());
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
            Car car = carStorage.getDetail();
            synchronized (syncObj) {
                syncObj.notifyAll(); // забираем тачку и будим воркер контроллер
            }
            Factory.logger.info("Dealer {} продал Auto {} (Body: {} Motor: {} Accessory: {}) ",
                    Thread.currentThread().getId(), car.getID(), car.body.getID(),
                    car.motor.getID(), car.accessory.getID());

        }
    }
}
