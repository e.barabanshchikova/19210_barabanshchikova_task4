package factory.workers;

import factory.Factory;
import factory.components.Body;
import factory.storage.Storage;

import java.util.concurrent.TimeUnit;

public class BodySupplier extends Supplier<Body> {

    public BodySupplier(Storage<Body> curStorage) {
        super(curStorage);
    }

    @Override
    public Body create() {
        try {
            TimeUnit.SECONDS.sleep(Factory.timeSet.getTimeForBody());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new Body((int) (Math.random() * 1000));
    }
}
