package factory.workers;

import factory.components.Detail;
import factory.storage.Storage;

public abstract class Supplier<T extends Detail> implements Runnable {
    private final Storage<T> storage;

    public abstract T create();

    public Supplier(Storage<T> curStorage) {
        storage = curStorage;
    }

    @Override
    public void run() {
        while (true) {
            storage.setDetail(create());
        }
    }
}

