package factory.workers;

import factory.Factory;
import factory.components.Accessory;
import factory.storage.Storage;

import java.util.concurrent.TimeUnit;

public class AccessorySupplier extends Supplier<Accessory> {

    public AccessorySupplier(Storage<Accessory> curStorage) {
        super(curStorage);
    }

    @Override
    public Accessory create() {
        try {
            TimeUnit.SECONDS.sleep(Factory.timeSet.getTimeForAccessory()); // время (задержка), затрачиваемое на создание
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new Accessory((int) (Math.random() * 1000));
    }
}
