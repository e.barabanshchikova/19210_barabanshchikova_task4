package factory;

import factory.components.Accessory;
import factory.components.Body;
import factory.components.Car;
import factory.components.Motor;
import factory.storage.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class View extends JFrame implements ActionListener {
    private final Storage<Body> bodyStorageInfo;
    private final Storage<Accessory> accessoryStorageInfo;
    private final Storage<Motor> motorStorageInfo;
    private final Storage<Car> carStorageInfo;

    private JPanel panelForProgressBars;
    private JPanel panelForCountOfCars;
    private JPanel panelForSpeedOfWorkers;

    private JLabel labelForCountOfCars;

    private JPanel panelForBody, panelForAccessory, panelForMotor, panelForCar;
    private JLabel labelForBody, labelForAccessory, labelForMotor, labelForCar;
    private JProgressBar bodyStoreProgress, accessoryStoreProgress, motorStoreProgress, carStoreProgress;

    private JPanel panelForTitle, panelForSuppliersAccessory, panelForCarCreator, panelForSuppliersMotor, panelForSuppliersBody, panelForDealer;
    private JLabel labelForSupplierMotor, labelForSupplierBody, labelForSupplierAccessory, labelForCarCreator, labelForDealer;
    private JLabel labelForTitleOfSpeeds;
    private JButton plusSpeedMotorButton, minusSpeedMotorButton, plusSpeedBodyButton, minusSpeedBodyButton,
            plusSpeedAccessoryButton, minusSpeedAccessoryButton, plusSpeedCarButton, minusSpeedCarButton,
            minusSpeedDealerButton, plusSpeedDealerButton;

    public View(Storage<Body> bodyStorageInfo, Storage<Accessory> accessoryStorageInfo,
                Storage<Motor> motorStorageInfo, Storage<Car> carStorageInfo) {
        super("Factory");
        this.accessoryStorageInfo = accessoryStorageInfo;
        this.bodyStorageInfo = bodyStorageInfo;
        this.carStorageInfo = carStorageInfo;
        this.motorStorageInfo = motorStorageInfo;
        initComponents();
        this.setVisible(true);
    }

    private void initComponents() {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension dimension = toolkit.getScreenSize();
        this.setBounds(dimension.width / 20, dimension.height / 20, 414, 620);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(null);
        this.setLayout(null);

        Font fontText = new Font("Ink Free", Font.BOLD, 16);
        Font fontTitle = new Font("Arial", Font.BOLD, 18);

        //создаем панели для количества машин, показателей заполненности складов и скорости работы работников
        panelForCountOfCars = new JPanel();
        panelForProgressBars = new JPanel();
        panelForSpeedOfWorkers = new JPanel();

        //устанавливаем размеры панелей
        panelForCountOfCars.setBounds(0, 0, 400, 75);
        panelForProgressBars.setBounds(0, 75, 400, 250);
        panelForSpeedOfWorkers.setBounds(0, 325, 400, 265);

        //Устанавливаем Layout
        panelForProgressBars.setLayout(null);
        panelForSpeedOfWorkers.setLayout(null);
        panelForCountOfCars.setLayout(null);

        //Установим задний фон для каждой панели
        panelForCountOfCars.setBackground(new Color(147, 138, 228));
        panelForProgressBars.setBackground(new Color(116, 110, 221));
        panelForSpeedOfWorkers.setBackground(new Color(90, 97, 224));

        //Заполним панель количества произведенных машин
        labelForCountOfCars = new JLabel();
        labelForCountOfCars.setBounds(15, 5, 370, 65);
        labelForCountOfCars.setFont(fontTitle);
        labelForCountOfCars.setText("Count of created cars: " + carStorageInfo.getCountOfCreatedDetails() + ", in storage: " + carStorageInfo.getSize());
        panelForCountOfCars.add(labelForCountOfCars);

        //Заполним панель прогресса каждого из работников
        //Сначала создадим 4 панели под каждый прогресс
        panelForBody = new JPanel();
        panelForAccessory = new JPanel();
        panelForCar = new JPanel();
        panelForMotor = new JPanel();

        panelForBody.setBackground(new Color(116, 110, 221));
        panelForAccessory.setBackground(new Color(116, 110, 221));
        panelForCar.setBackground(new Color(116, 110, 221));
        panelForMotor.setBackground(new Color(116, 110, 221));

        //Зададим размеры панелей для каждого из прогрессов
        panelForMotor.setBounds(10, 5, 380, 60);
        panelForAccessory.setBounds(10, 65, 380, 60);
        panelForBody.setBounds(10, 125, 380, 60);
        panelForCar.setBounds(10, 185, 380, 60);

        carStoreProgress = new JProgressBar();
        motorStoreProgress = new JProgressBar();
        accessoryStoreProgress = new JProgressBar();
        bodyStoreProgress = new JProgressBar();

        //сделаем отображение прогресса в процентах
        carStoreProgress.setStringPainted(true);
        motorStoreProgress.setStringPainted(true);
        accessoryStoreProgress.setStringPainted(true);
        bodyStoreProgress.setStringPainted(true);

        //установим шрифт для записи процентов
        carStoreProgress.setFont(fontText);
        motorStoreProgress.setFont(fontText);
        accessoryStoreProgress.setFont(fontText);
        bodyStoreProgress.setFont(fontText);

        //установим цвета для каждого из баров
        carStoreProgress.setForeground(new Color(153, 196, 122));
        bodyStoreProgress.setForeground(new Color(153, 196, 122));
        motorStoreProgress.setForeground(new Color(153, 196, 122));
        accessoryStoreProgress.setForeground(new Color(153, 196, 122));

        //добавим подписи к каждому бару
        labelForAccessory = new JLabel();
        labelForBody = new JLabel();
        labelForCar = new JLabel();
        labelForMotor = new JLabel();

        labelForAccessory.setBounds(0, 0, 100, 60);
        labelForBody.setBounds(0, 0, 100, 60);
        labelForCar.setBounds(0, 0, 100, 60);
        labelForMotor.setBounds(0, 0, 100, 60);

        labelForCar.setFont(fontText);
        labelForCar.setText("Car Store");
        labelForAccessory.setFont(fontText);
        labelForAccessory.setText("Accessory Store");
        labelForMotor.setFont(fontText);
        labelForMotor.setText("Motor Store");
        labelForBody.setFont(fontText);
        labelForBody.setText("Body Store");

        //зададим размеры каждого бара
        carStoreProgress.setBounds(0, 0, 280, 60);
        motorStoreProgress.setBounds(0, 0, 280, 60);
        accessoryStoreProgress.setBounds(0, 0, 280, 60);
        bodyStoreProgress.setBounds(0, 0, 280, 60);

        //добавим в каждую панель по бару и подписи:
        panelForCar.add(labelForCar);
        panelForCar.add(carStoreProgress);

        panelForBody.add(labelForBody);
        panelForBody.add(bodyStoreProgress);

        panelForAccessory.add(labelForAccessory);
        panelForAccessory.add(accessoryStoreProgress);

        panelForMotor.add(labelForMotor);
        panelForMotor.add(motorStoreProgress);

        //Теперь каждую из панелей добавим в соответствующую панель
        panelForProgressBars.add(panelForAccessory);
        panelForProgressBars.add(panelForBody);
        panelForProgressBars.add(panelForMotor);
        panelForProgressBars.add(panelForCar);

        //Сделаем теперь еще панели для каждой из панелей скоростей:
        panelForTitle = new JPanel();
        panelForSuppliersAccessory = new JPanel();
        panelForSuppliersBody = new JPanel();
        panelForSuppliersMotor = new JPanel();
        panelForCarCreator = new JPanel();
        panelForDealer = new JPanel();

        panelForTitle.setBounds(10, 10, 380, 40);
        panelForTitle.setBackground(new Color(90, 97, 224));
        panelForSuppliersMotor.setBounds(10, 50, 380, 40);
        panelForSuppliersMotor.setBackground(new Color(90, 97, 224));
        panelForSuppliersBody.setBounds(10, 90, 380, 40);
        panelForSuppliersBody.setBackground(new Color(90, 97, 224));
        panelForSuppliersAccessory.setBounds(10, 130, 380, 40);
        panelForSuppliersAccessory.setBackground(new Color(90, 97, 224));
        panelForCarCreator.setBounds(10, 170, 380, 40);
        panelForCarCreator.setBackground(new Color(90, 97, 224));
        panelForDealer.setBounds(10, 210, 380, 40);
        panelForDealer.setBackground(new Color(90, 97, 224));

        //Теперь сделаем кнопки и подписи для последней панели связанной со скоростями
        //Сначала делаем подписи
        labelForSupplierAccessory = new JLabel();
        labelForCarCreator = new JLabel();
        labelForSupplierBody = new JLabel();
        labelForSupplierMotor = new JLabel();
        labelForTitleOfSpeeds = new JLabel();
        labelForDealer = new JLabel();

        labelForTitleOfSpeeds.setText("Delay ");
        labelForTitleOfSpeeds.setFont(fontTitle);
        labelForTitleOfSpeeds.setBounds(0, 0, 380, 40);

        labelForSupplierBody.setText("For body: " + Factory.timeSet.getTimeForBody());
        labelForCarCreator.setText("For car: " + Factory.timeSet.getTimeForMachine());
        labelForSupplierMotor.setText("For motor: " + Factory.timeSet.getTimeForMotor());
        labelForSupplierAccessory.setText("For accessory: " + Factory.timeSet.getTimeForAccessory());
        labelForDealer.setText("For Dealers: " + Factory.timeSet.getTimeForSaleCar());

        labelForCarCreator.setFont(fontText);
        labelForSupplierMotor.setFont(fontText);
        labelForSupplierAccessory.setFont(fontText);
        labelForSupplierBody.setFont(fontText);
        labelForDealer.setFont(fontText);

        labelForSupplierBody.setBounds(0, 0, 150, 40);
        labelForSupplierAccessory.setBounds(0, 0, 150, 40);
        labelForSupplierMotor.setBounds(0, 0, 150, 40);
        labelForCarCreator.setBounds(0, 0, 150, 40);
        labelForDealer.setBounds(0, 0, 150, 40);

        //Теперь добавим кнопки
        plusSpeedAccessoryButton = new JButton();
        plusSpeedBodyButton = new JButton();
        plusSpeedCarButton = new JButton();
        plusSpeedMotorButton = new JButton();
        plusSpeedDealerButton = new JButton();

        minusSpeedBodyButton = new JButton();
        minusSpeedCarButton = new JButton();
        minusSpeedMotorButton = new JButton();
        minusSpeedAccessoryButton = new JButton();
        minusSpeedDealerButton = new JButton();

        minusSpeedDealerButton.setBounds(155, 5, 90, 30);
        minusSpeedDealerButton.setText("-1");
        minusSpeedDealerButton.addActionListener(this);
        plusSpeedDealerButton.setBounds(255, 5, 90, 30);
        plusSpeedDealerButton.setText("+1");
        plusSpeedDealerButton.addActionListener(this);

        minusSpeedMotorButton.setBounds(155, 5, 90, 30);
        minusSpeedMotorButton.setText("-1");
        minusSpeedMotorButton.addActionListener(this);
        plusSpeedMotorButton.setBounds(255, 5, 90, 30);
        plusSpeedMotorButton.setText("+1");
        plusSpeedMotorButton.addActionListener(this);

        minusSpeedCarButton.setBounds(155, 5, 90, 30);
        minusSpeedCarButton.setText("-1");
        minusSpeedCarButton.addActionListener(this);
        plusSpeedCarButton.setBounds(255, 5, 90, 30);
        plusSpeedCarButton.setText("+1");
        plusSpeedCarButton.addActionListener(this);

        minusSpeedBodyButton.setBounds(155, 5, 90, 30);
        minusSpeedBodyButton.setText("-1");
        minusSpeedBodyButton.addActionListener(this);
        plusSpeedBodyButton.setBounds(255, 5, 90, 30);
        plusSpeedBodyButton.setText("+1");
        plusSpeedBodyButton.addActionListener(this);

        minusSpeedAccessoryButton.setBounds(155, 5, 90, 30);
        minusSpeedAccessoryButton.setText("-1");
        minusSpeedAccessoryButton.addActionListener(this);
        plusSpeedAccessoryButton.setBounds(255, 5, 90, 30);
        plusSpeedAccessoryButton.setText("+1");
        plusSpeedAccessoryButton.addActionListener(this);

        //Добавляем кнопки и подписи в соответствующие панели
        panelForSuppliersAccessory.add(labelForSupplierAccessory);
        panelForSuppliersAccessory.add(plusSpeedAccessoryButton);
        panelForSuppliersAccessory.add(minusSpeedAccessoryButton);

        panelForDealer.add(labelForDealer);
        panelForDealer.add(plusSpeedDealerButton);
        panelForDealer.add(minusSpeedDealerButton);

        panelForTitle.add(labelForTitleOfSpeeds);

        panelForCarCreator.add(labelForCarCreator);
        panelForCarCreator.add(plusSpeedCarButton);
        panelForCarCreator.add(minusSpeedCarButton);

        panelForSuppliersBody.add(labelForSupplierBody);
        panelForSuppliersBody.add(plusSpeedBodyButton);
        panelForSuppliersBody.add(minusSpeedBodyButton);

        panelForSuppliersMotor.add(labelForSupplierMotor);
        panelForSuppliersMotor.add(plusSpeedMotorButton);
        panelForSuppliersMotor.add(minusSpeedMotorButton);

        //Добавляем эти панели в панель скоростей
        panelForSpeedOfWorkers.add(panelForCarCreator);
        panelForSpeedOfWorkers.add(panelForSuppliersAccessory);
        panelForSpeedOfWorkers.add(panelForSuppliersBody);
        panelForSpeedOfWorkers.add(panelForSuppliersMotor);
        panelForSpeedOfWorkers.add(panelForTitle);
        panelForSpeedOfWorkers.add(panelForDealer);

        //И наконец добавляем все панели в фрэйм
        this.add(panelForCountOfCars);
        this.add(panelForProgressBars);
        this.add(panelForSpeedOfWorkers);
    }

    void fill() {
        labelForSupplierBody.setText("For body: " + Factory.timeSet.getTimeForBody());
        labelForCarCreator.setText("For car: " + Factory.timeSet.getTimeForMachine());
        labelForSupplierMotor.setText("For motor: " + Factory.timeSet.getTimeForMotor());
        labelForSupplierAccessory.setText("For accessory: " + Factory.timeSet.getTimeForAccessory());
        labelForDealer.setText("For Dealers: " + Factory.timeSet.getTimeForSaleCar());
        labelForCountOfCars.setText("Count of created cars: " + carStorageInfo.getCountOfCreatedDetails() + ", in storage: " + carStorageInfo.getSize());
        carStoreProgress.setValue((int) ((double) carStorageInfo.getSize() / carStorageInfo.getMaxSize() * 100));
        bodyStoreProgress.setValue((int) ((double) bodyStorageInfo.getSize() / bodyStorageInfo.getMaxSize() * 100));
        accessoryStoreProgress.setValue((int) ((double) accessoryStorageInfo.getSize() / accessoryStorageInfo.getMaxSize() * 100));
        motorStoreProgress.setValue((int) ((double) motorStorageInfo.getSize() / motorStorageInfo.getMaxSize() * 100));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == plusSpeedAccessoryButton) {
            Factory.timeSet.setTimeForAccessory(Factory.timeSet.getTimeForAccessory() + 1);
        }
        if (e.getSource() == plusSpeedBodyButton) {
            Factory.timeSet.setTimeForBody(Factory.timeSet.getTimeForBody() + 1);
        }
        if (e.getSource() == plusSpeedCarButton) {
            Factory.timeSet.setTimeForMachine(Factory.timeSet.getTimeForMachine() + 1);
        }
        if (e.getSource() == plusSpeedMotorButton) {
            Factory.timeSet.setTimeForMotor(Factory.timeSet.getTimeForMotor() + 1);
        }
        if (e.getSource() == plusSpeedDealerButton) {
            Factory.timeSet.setTimeForSaleCar(Factory.timeSet.getTimeForSaleCar() + 1);
        }
        if (e.getSource() == minusSpeedAccessoryButton && Factory.timeSet.getTimeForAccessory() > 1) {
            Factory.timeSet.setTimeForAccessory(Factory.timeSet.getTimeForAccessory() - 1);
        }
        if (e.getSource() == minusSpeedBodyButton && Factory.timeSet.getTimeForBody() > 1) {
            Factory.timeSet.setTimeForBody(Factory.timeSet.getTimeForBody() - 1);
        }
        if (e.getSource() == minusSpeedCarButton && Factory.timeSet.getTimeForMachine() > 1) {
            Factory.timeSet.setTimeForMachine(Factory.timeSet.getTimeForMachine() - 1);
        }
        if (e.getSource() == minusSpeedMotorButton && Factory.timeSet.getTimeForMotor() > 1) {
            Factory.timeSet.setTimeForMotor(Factory.timeSet.getTimeForMotor() - 1);
        }
        if (e.getSource() == minusSpeedDealerButton && Factory.timeSet.getTimeForAccessory() > 1) {
            Factory.timeSet.setTimeForSaleCar(Factory.timeSet.getTimeForSaleCar() - 1);
        }
    }
}
